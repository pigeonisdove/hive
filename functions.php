<?php

/*
 * Register parent theme styles instead of css @import
 */

function hive_styles() {

	$parent = 'hexa-style';
	$theme = wp_get_theme();
	wp_enqueue_style( 'hive-style', get_stylesheet_uri(),
		array( $parent ),
		$theme->get('Version')
	);
	wp_enqueue_style( $parent, get_template_directory_uri() . '/style.css',
		array(),
		$theme->parent()->get('Version')
	);

}

add_action( 'wp_enqueue_scripts', 'hive_styles' );

/*
 * Adds footer menu
 */

function register_footer_menu () {
	register_nav_menu('footer-menu',__( 'Footer Menu' ));
}

add_action( 'init', 'register_footer_menu' );
