# Hive

<img src="screenshot.png" alt="Hive Child Theme Screenshot" width="300">

[Hive](https://gitlab.com/pigeonisdove/hive) is a WordPress child theme based on [Hexa](https://wordpress.com/theme/hexa) (by Automattic). The focus is for bee-related blogs and web sites, for beekeepers and other bee-fans. 

## Child Themes

The hexogonal elements in Hexa, along with the simple and readable design, are an excellent nod toward the natural shape of bee hives--the honeycomb made of bee's wax. Hive allows for further customization of the parent theme, Hexa, than would normally be possible with the Theme Customizer feature. Plus, creating a child theme makes such customizations more permanent, allowing for easier transportability and deployment of the site design (separating it from the database). Read more about child themes in the [WordPress developer docs](https://developer.wordpress.org/themes/advanced-topics/child-themes/). 


## Usage

From your `wordpress` directory, simply issue the following terminal commands:

```bash
cd wp-content/themes
curl -L https://downloads.wordpress.org/theme/hexa.1.0.5.zip -o hexa.zip
unzip hexa.zip
git clone https://gitlab.com/pigeonisdove/hive.git
```

If you're not able to use the command line interface, you can use whatever graphic programs you like instead.

Simply [download a zip file](https://gitlab.com/pigeonisdove/hive/-/archive/master/hive-master.zip), unzip it, and copy the `hive` folder to your `themes` directory such as with a graphical file browser or an SFTP client. Do the same thing with the parent theme, Hexa ([zip](https://downloads.wordpress.org/theme/hexa.1.0.5.zip)).

## Try with Docker-Compose

If you want to try this theme out relatively quickly on your own machine without a server, you can [use Docker with Docker-Compose](https://docs.docker.com/compose/wordpress/). I modified the configuration to make the theme and plugin directories locally accessible, which you can see in the ```docker-compose.yml``` file copied below.

```yaml
version: '3.3'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
       WORDPRESS_DB_NAME: wordpress
     working_dir: /var/www/html
     volumes:
       - ./wp-content/themes:/var/www/html/wp-content/themes/
       - ./wp-content/plugins:/var/www/html/wp-content/plugins
volumes:
    db_data: {}
```

## Contributing

Pull requests, bug reports, and questions welcome.

## License
[GPL (Gnu General Public License) v2.0](https://choosealicense.com/licenses/gpl-2.0/)
