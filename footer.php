<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Hexa
 */
?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="sub-footer">
			<nav id="footer-nav" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container-class' => 'main-navigation' ) ); ?>
			</nav><!-- #footer-nav -->
		</div><!-- #sub-footer -->
		<div class="site-info">
			<?php // do_action( 'hexa_credits' ); ?>
			<!--<a href="http://wordpress.org/" rel="generator"><?php printf( __( 'Proudly powered by %s', 'hexa' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>-->
			<?php printf( __( 'Theme: %1$s based on %2$s ', 'hive' ), '<a href="https://github.com/wrought/hive">Hive</a>', '<a href="http://wordpress.com/themes/hexa/" rel="designer">Hexa</a>' ); 
			// printf( __( 'Theme: %1$s by %2$s.', 'hexa' ), 'Hexa', '<a href="http://wordpress.com/themes/hexa/" rel="designer">WordPress.com</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
